import org.junit.jupiter.api.Test;


public class FirstTest extends BaseTest {
    @Test
    public void makeUpOpen() {
        driver.get(Variables.firstPageUrl);
        makeUpStartPage.setSearchElementId(Variables.elementId);
        makeUpStartPage.inputTextIntoSearchField(Variables.searchInputRoller);
        makeUpStartPage.submitSearchButton();
        makeUpStartPage.openFoundElementFromSearchList();
        assert makeUpStartPage.getProductItemNameText().equals(Variables.mezoRollerItemName);
        System.out.println("Title matches with expected!!!!");
    }
}
