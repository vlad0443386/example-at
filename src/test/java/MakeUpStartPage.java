import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class MakeUpStartPage extends BasePage {
    public MakeUpStartPage(WebDriver driver) {
        super(driver);
    }

    private String searchElement;

    @FindBy(xpath = "//button[@class='search-button'][@type='submit']")
    private WebElement submitSearchButton;

    @FindBy(xpath = "//input[@itemprop='query-input'][@type='search']")
    private WebElement searchInput;

    @FindBy(xpath = "//div[@class='product-item__name']")
    private WebElement productItemName;


    public String getProductItemNameText() {
        waitVisibilityOfElement(productItemName);
        return productItemName.getText();
    }

    public void inputTextIntoSearchField(String searchString) {
        waitVisibilityOfElement(searchInput);
        searchInput.click();
        searchInput.sendKeys(searchString);
    }

    public void setSearchElementId(String elementId) {
        searchElement = ("//li[@data-id='" + elementId + "'][@data-price]");
    }

    public void submitSearchButton() {
        waitUntilElementIsClickable(submitSearchButton);
        submitSearchButton.click();
    }

    public void openFoundElementFromSearchList() {
        waitVisibilityOfElement(driver.findElement(By.xpath(searchElement)));
        driver.findElement(By.xpath(searchElement)).click();
    }
}
