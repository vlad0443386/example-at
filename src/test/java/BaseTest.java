import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected static WebDriver driver;
    public BasePage basePage;
    public MakeUpStartPage makeUpStartPage;


    @BeforeEach
    public void setUp() throws IOException {
        driverAndPagesSetup();
    }

    public static void driverSetup(String driverUrl) throws IOException {
        driver = new SeleniumDriverFactory().createDriver(driverUrl);
        driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    public void driverAndPagesSetup() throws IOException {
        driverSetup(Variables.driverURL);
        basePage = new BasePage(driver);
        makeUpStartPage = new MakeUpStartPage(driver);
    }

    @AfterEach
    public void close() {
        driver.close();
        driver.quit();
    }

}
